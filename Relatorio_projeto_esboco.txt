---------------------------------------------------INTRODUÇÃO----------------------------------------------------------------
Ophiocordyceps unilateralis é um fungo parasita especializado. O seu modo de sobrevivência e reprodução baseia-se em infetar, manipular e por fim matar formigas carpinteiras, predominantemente em ecossistemas de florestas tropicais.
Este fungo começa por libertar os seus esporos. Quando um desses esporos entra em contacto com o exoesqueleto de uma formiga este tem a capacidade de o penetrar, evitando ou suprimindo as defesas da formiga. No interior da formiga, o fungo é capaz de controlar ou alterar o seu comportamento. Eles "enchem" os cérebros das formigas com produtos químicos (toxinas), de forma a drogá-las e a obrigá-las a deslocarem-se para locais que tenham condições perfeitas, como uma boa quantidade de luz e humidade, para que o parasita se desenvolva dentro do hospedeiro. Numa fase terminal de vida da formiga, esta é então forçada a subir ao topo de um ramo de uma planta ou de uma árvore onde faz a sua mordida mortal numa nervura de uma folha ou num galho. As mandíbulas da formiga exercem uma força consideravelmente grande o suficiente para que a formiga fique agarrada na folha. Após algumas semanas da morte da formiga, e do crescimento do fungo associado a este acontecimento, o fungo que cresceu na região dorsal do pescoço da formiga pode libertar os seus esporos e o processo repete-se continuamente. 
Os esporos têm ainda a capacidade de proteger os cadáveres das formigas de ataques microbianos e necrófagos. 
Os fungos do género Cordyceps podem dizimar colónias de formigas inteiras. Existem mais de 600 espécies de Cordyceps espalhadas pelo mundo inteiro, encontrando-se a maior parte delas em selvas. 

------------------------------------------------OBJETIVOS-------------------------------------------------------------------
Com este projeto espera-se conseguir obter e visualizar as estruturas 3D das proteínas das espécies em estudo através de métodos de previsão computacional.
Pretende-se perceber como é que as duas proteínas interagem e como é que isso se relaciona com os efeitos patogénicos que as formigas apresentam.
--------------------------------------------------METODOLOGIA--------------------------------------------------------------- 
2.1 - Obter as sequências das proteínas hipotéticas de ambas as espécies em estudo
2.2 - Determinar as suas respetivas estruturas 3D
2.3 - Validação das estruturas
2.4 - Análise das interações entre as proteínas
2.5 - Automatização dos passos anteriores



----------------------------------------------------DISCUSSÃO---------------------------------------------------------------
Com o objetivo de perceber como ocorrem as interações entre as duas espécies e qual é a relação dessas interações com o comportamento da formiga utilizaram-se os métodos de modelagem comparativa e de docking.
A modelagem comparativa serve para prever a estrutura 3D de uma proteína com base na estrutura 3D de uma outra proteína similar (molde) que é usada como referência.
Para isso então começou-se por recolher dois ficheiros fasta tanto da formiga (Camponotus tortuganus) como do fungo(Ophiocordyceps unilateralis) que continham as respetivas sequências de proteínas.
Fez-se um blast para ambos os ficheiros fasta, com auxílio do software blastp do NCBI para obtermos as proteínas que serão usadas como referência. Selecionaram-se as proteínas com os melhores resultados:
6vlt - proteína usada como referência para o fungo, 7o37 - proteína usada como referência para a formiga. 
A proteína 6vlt apresentou uma percentagem de identidade de 27,89% e uma query cover de 82%. Já a 7o37 tem uma percentagem de identidade de 71,10% e uma query cover de 99%. Percebemos então que podemos avançar na construção do modelo tendo em conta que na modelagem comparativa é necessário ter pelo menos 30% de query cover e identidade (apesar da percentagem de identidade do fungo ser ligeiramente abaixo dos 30% não é significativo para o bom uso do método).
Ainda assim observa-se aqui alguma discrepância na semelhança da proteína do fungo, sendo que a proteína associada à formiga apresenta uma maior semelhança. 

******************************************Colocar as prints do blastp********************************************************
Para a construção dos modelos das proteínas hipotéticas, colocaram-se os ficheiros fasta (7o37, 6vlt) obtidos do blastp no software Swiss-Model. 
Obteve-se então dessa construção os ficheiros pdb: ModeloResultadoformiga.pbd e ModeloResultadofungo.pdb.
De seguida procurou-se os modelos das proteínas de referências (7o37 ,6vlt) no RCSB-PDB, um site que contém uma base de dados de proteínas (7o37.pdb, 6vlt.pdb).

******************************************Colocar as prints dos modelos******************************************************
Após a construção dos modelos é necessário validá-los caso contrário o docking poderá apresentar informações erradas.
Para validar o modelo fez-se uso das técnicas do gráfico de Ramachandran e RMSD.
O gráfico de Ramachandran mostra a distribuição estatística das combinações dos ângulos diédricos dos ângulos phi ϕ e psi ψ. Em teoria, as regiões permitidas neste gráfico apresentam quais os valores dos ângulos phi/psi que são possíveis um aminoácido ter, tendo em conta a sua estrutura secundária: hélice-alfa, folha-beta e regiões de loop.
Os pdb's utilizados para a construção foram o ModeloResultadofungo.pdb e ModeloResultadoformiga.pdb. 

*****************************************Colocar as prints dos gráficos Ramachandran*****************************************

No gráfico relativo à formiga observam-se alguns aminoácidos nas zonas relativas às folhas beta (folhas-beta antiparalelas, folhas-beta paralelas, triplas hélices de colagénio e folhas-beta "right-twisted").
Existe uma quantidade bastante pequena de pontos na zona relativa à hélice-alfa "left-handed".
Encontram-se bastantes aminoácidos presentes na região da hélice-alfa "right-handed" (ou seja, bastantes aminoácidos presentes em diferentes hélices-alfa "right-handed"), tornando esta estrutura secundária a mais prevalente na proteína.
O modelo da estrutura da proteína da formiga não apresentou aminoácidos em regiões não permitidas, o que evidencia um bom modelo da estrutura da proteína. 94.7% dos aminoácidos encontram-se em regiões mais favoráveis e 5.3% dos aminoácidos encontram-se em regiões adicionais permitidas. 

Já o gráfico relativo ao fungo apresenta muitos pontos nas zonas relativas às folhas-beta (folhas-beta antiparalelas, folhas-beta paralelas, triplas hélices de colagénio e folhas-beta "right-twisted"). 
Poucos pontos na zona relativa à hélice-alfa "left-handed".
Existem bastantes aminoácidos presentes na região da hélice-alfa "right-handed" (ou seja, bastantes aminoácidos presentes em diferentes hélices-alfa "right-handed"), tornando esta estrutura secundária a mais prevalente na proteína, pois há uma densidade maior de pontos nesta área comparando com a zona das folhas-beta. 
O modelo da estrutura da proteína apresentou 4 aminoácidos em regiões não permitidas: uma histidina (HIS), um ácido glutâmico (GLU), um glutamina (GLN) e uma serina (SER), que perfazem 1% dos resíduos da proteína. Estas regiões não permitidas possuem os valores dos ângulos torsionais inadequados para a constituição da proteína modelada, no entanto, resíduos de glicina e prolina são exceções para esta área. Estes dois resíduos apresentam variações na cadeia lateral que conferem maior rigidez, no caso da prolina, e maior flexibilidade, no caso da glicina, o que lhes permite assim assumir ângulos não esperados. 
Ainda assim nenhum destes aminoácidos referidos fazem parte das 5 interações existentes no docking entre a proteína da formiga e a proteína do fungo. 86.1% dos aminoácidos estão em regiões mais favoráveis, 12.4% dos aminoácidos estão em regiões adicionais permitidas e 0.5% dos aminoácidos estão em regiões generosamente permitidas.

O RMSD dá o desvio (quadrático) médio entre os átomos correspondentes de duas proteínas. É basicamente uma medida de distância entre moléculas, muito usada para medir a distância ou diferença entre estruturas de proteínas sobrepostas. O cálculo do RMSD pode ser aplicado noutras moléculas que não sejam proteínas, tal como, pequenas moléculas orgânicas. Uma maneira muito utilizada para comparar estruturas de biomoléculas ou corpos sólidos é mover e rodar uma estrutura em relação à outra para minimizar o RMSD. Quanto mais baixo for o valor, mais similares são as proteínas. Para o cálculo do valor do RMSD, é comum que se tomem todos os átomos de uma estrutura, ou apenas os carbonos alfa, ou apenas os carbonos, nitrogénios e oxigénios do backbone da proteína (backbone é o que mantém uma proteína unida e lhe dá uma forma geral- estrutura terciária). Ou seja, há várias formas de calcular o valor de RMSD de uma proteína, mas a equação é sempre a mesma.

**********************************Colocar a equação do RMSD que se encontra no chat do grupo********************************
Utilizou-se o software PyMOL para se utilizar a validação por RMSD.
A partir dos valores obtidos de RMSD entre as proteínas de referência e as proteínas hipotéticas, tanto para o fungo, como para a formiga, podemos dizer que são estruturas bastante idênticas: quanto às proteínas de fungo foi obtido um valor de RMSD de 0.295 Å (para um total de 403 átomos) e quanto às proteínas da formiga obteve-se um valor de RMSD de 0.052 Å (para um total de 492 átomos).

Com os modelos validados está tudo pronto para se fazer o docking das duas proteínas. O docking foi feito no software HDock. Foi obtido um ficheiro pdb que contém o docking que foi posteriormente analisado no PyMOL. 
Instruções feitas para se observar as interações no PyMOL:
	1- selecionar uma das proteínas --> A (Action)--> find --> polar contacts --> to any atoms
	2- S (Show)-->labels (nas interações)

********************************Colocar a imagem geral das interações********************************************************
********************************Colocar a tabela das interações**************************************************************

Para se observar melhor as interações entre os aminoácidos fizeram-se os seguintes passos no PyMOL:
1- S-->lines (em ambas as proteínas)
2- Selecionar os aminoácidos que fazem parte da interação
3- Ver as cadeias de aminoácidos das duas proteínas e ver os aminoácidos selecionados que são os que estão em interação 
4- S --> as --> cartoon nas duas proteínas
5- S --> lines --> nas duas proteínas
6- S --> as --> sticks no (sele)
7- L --> residues no (sele)
8- (sele)-->action-->copy to object
9- (sele)-->hide-->sticks
10- setting-->transparency-->surface-->60%

O resultado do docking apresenta 5 interações entre o fungo e a proteína:
1 - Interação entre uma isoleucina (I) da proteína do fungo e um triptofano (W) da proteína da formiga, com uma distância de 3.4 angstroms.
É uma interação entre um aminoácido não polar e um aminoácido aromático, relativamente não polar.

2 - Interação entre uma lisina (K) da proteína do fungo e uma fenilalanina (F) da proteína da formiga, com uma distância de 2.1 angstroms.
É uma interação entre um aminoácido polar básico e um aminoácido aromático, relativamente não polar.

3 - Interação entre uma treonina (T) da proteína do fungo e um triptofano (W) da proteína da formiga, com uma distância de 2.9 angstroms.
É uma interação entre um aminoácido polar e um aminoácido aromático, relativamente não polar.

4 - Interação entre um ácido aspártico (D) da proteína do fungo e uma arginina (R) da proteína da formiga, com uma distância de 3.3 angstroms.
É uma interação entre um aminoácido polar ácido e um aminoácido polar básico.

5 - Interação entre um ácido glutâmico (E) da proteína do fungo e uma fenilalanina (F) da proteína da formiga, com uma distância de 3.5 angstroms.
É uma interação entre um aminoácido polar ácido e um aminoácido aromático, relativamente não polar.


***********************************************Print das interações*********************************************************
 
O núcleo da isoleucina é o mais hidrófobo de todos os radicais dos aminoácidos das proteínas. Tais características hidrofóbicas permitem a formação de ligações hidrófobas que são consideradas interações químicas fracas.
A lisina contém uma cadeia lateral muito polar, o que a torna altamente hidrofílica. Este aminoácido tem carga positiva em pH neutro.
O triptofano e a fenilalanina são aminoácidos aromáticos que formam ligações π que são geralmente fortes.


Iremos discutir de seguida a presença de interações não covalentes:

Na 1ª interação observamos a ligação entre o grupo carboxilo da isoleucina e o anel aromático com nitrogénio (N) do triptofano. Essa ligação é formada através duma ponte de hidrogénio (O-H-----N). Estão também presentes nesta interação forças de dispersão de London. Entre as duas ligações, a ponte de hidrogénio é a mais forte.

No caso da segunda interação temos uma ligação não covalente do tipo catião - π, devido à cadeia lateral básica, carregada positivamente da lisina e do anel aromático presente na fenilalanina, uma ponte de hidrogénio, e interações de Debye e de London. Temos duas ligações não covalentes relativamente fortes: catião - π e ponte de hidrogénio. 

Na 3ª interação observamos uma ponte de hidrogénio entre uma treonina e um triptofano. Estão também presentes nesta interação forças de Debye e forças de dispersão de London. Entre estas ligações, a ponte de hidrogénio é a mais forte.

Na 4º interação existe uma ligação iónica (a ligação não covalente mais forte), pois o ácido aspártico tem carga negativa e a arginina tem carga positiva, uma ponte de hidrogénio, e interações de Van der Waals (Keesom, Debye e London), sendo a ligação iónica a mais forte.

Na quinta interação ocorre uma ligação anião - π devido à cadeia lateral ácida, carregada negativamente do ácido glutâmico, e do anel aromático presente na fenilalanina. Além disso temos uma ponte de hidrogénio, e forças de Debye e de London. A ponte de hidrogénio é a mais forte. 

O docking foi feito uma segunda vez no software Hex, onde se obteve um valor de ΔG (Etotal) favorável ao conjunto de interações químicas observáveis: -771,65.

***********************************************Print do docking feito no Hex*********************************************************

De acordo com as características dos aminoácidos envolvidos nas interações, e tendo em conta o número de interações encontradas no docking, o número de interações entre os aminoácidos, e as distâncias entre estes mesmos aminoácidos (menores de 4 angstrons), pode-se concluir que as afinidades químicas são fortes o suficientes para confirmar essa predição de interação proteína-proteína. 
Como o valor de ΔG é negativo, o processo pode ser descrito como espontâneo, ou seja, não há necessidade de haver gasto de energia para que aquela interação aconteça. As ligações feitas entre os aminoácidos são mais rígidas e não se quebram facilmente, o que evidencia a possibilidade desta interação ser real. 
Baseado nas análises de predição computacional, espera-se que as interações químicas exercidas entre as duas proteínas sejam realizadas entre este conjunto de aminoácidos. 

O resultado de um estudo apresentado num artigo científico [1] revelou que depois da manipulação da formiga, o O. unilateralis sl tinha expressado uma grande quantidade de genes envolvidos em processos de oxidação-redução, como o citocromo P450s. Um dos muitos papéis desempenhados pelo citocromo P450s envolve a biossíntese de metabolitos secundários relacionados à patogénese. Descobriu-se também que o citocromo P450s foi regulado positivamente durante a mordida manipulada.

"(...) entre os 391 genes regulados negativamente na cabeça do hospedeiro vivo durante a mordida manipulada, aqueles relacionados à atividade da oxidorredutase estão presentes em grande quantidade. Isso sugere que o O. unilateralis sl pode ser capaz de suprimir as respostas imunes do hospedeiro na cabeça durante o comportamento de mordida manipulada.(...)".

Foi também dito que "(...) Genes supostamente envolvidos em respostas imunes e de stress foram geralmente regulados negativamente nas cabeças das formigas infetadas durante o acontecimento de mordida manipulada.".

Referiu-se que "De facto, os genes relacionados à atividade da oxidorredutase foram enriquecidos no subconjunto de genes de formigas regulados negativamente. As reações de oxidação-redução são mecanismos vitais nas interações parasita-hospedeiro, tanto para o parasita quanto para o hospedeiro. Como tal, no parasita fúngico, de facto, encontramos genes envolvidos nesses processos sendo regulados positivamente durante a manipulação e regulados negativamente novamente depois que o hospedeiro foi presumivelmente morto. (...) Como sugerido para outras interações inseto-parasita, a supressão das respostas do hospedeiro pode, portanto, ser crucial para criar condições no hospedeiro que favoreçam o desenvolvimento do parasita. Além de causar apoptose e prejudicar a comunicação quimiossensorial, o O. unilateralis sl pode, portanto, estar a regular as respostas de stress imune e neuronal do hospedeiro como um mecanismo para estabelecer comportamento manipulado. (...)".

---------------------------------------------------------Conclusão-----------------------------------------------------------

Neste projeto abordaram-se diferentes técnicas no âmbito da bioquímica computacional, técnicas essas que permitiram o estudo e a análise do funcionamento e das interações destas duas proteínas. Através destas técnicas, foi possível apurar fortes evidências que validam a existência desta interação proteína-proteína e a sua influência no comportamento manipulado da formiga pelo fungo. Estes resultados não correspondem necessariamente à realidade, pois só seria possível prová-los em laboratório, mas esta previsão computacional mostra que valeria a pena investir em estudos laboratoriais para que assim se pudesse confirmar a veracidade da previsão.  

----------------------------------------------------------Bibliografia-----------------------------------------------------

https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4545319/
https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3204140/
https://www.rcsb.org/
https://www.youtube.com/watch?v=vijGdWn5-h8&t=49s
https://saves.mbi.ucla.edu/
https://www.uniprot.org/uniprotkb/A0A2A9PKD1/entry
http://hdock.phys.hust.edu.cn/
https://blast.ncbi.nlm.nih.gov/Blast.cgi?PAGE=Proteins





