Com os modelos validados está tudo pronto para se fazer o docking das duas proteínas. Para tal também se fez uso do PyMOL.
Para se encontrar as interações foram feitas no PyMOL as seguintes instruções:
	1- selecionar uma das proteínas --> A (Action)--> find --> polar contacts --> to any atoms
	2- S (Show)-->labels (nas interações)

Para se observar melhor as interações entre os aminoácidos seguiram-se os seguintes passos no PyMOL:
1- S-->lines (em ambas as proteínas)
2- Selecionar os aminoácidos que fazem parte da interação
3- Ver as cadeias de aminoácidos das duas proteínas e ver os aminoácidos selecionados que são os que estão em interação 
4- S --> as --> cartoon nas duas proteínas
5- S --> lines --> nas duas proteínas
6- S --> as --> sticks no (sele)
7- L --> residues no (sele)
8- (sele)-->action-->copy to object
9- (sele)-->hide-->sticks
10- setting-->transparency-->surface-->60%

O resultado do docking apresenta 5 interações entre o fungo e a proteína:
1 - Interação entre uma isoleucina (I) da proteína do fungo e um triptofano (W) da proteína da formiga, com uma distância de 3.4 angstroms.
É uma interação entre um aminoácido não polar e um aminoácido aromático, relativamente não polar.

2 - Interação entre uma lisina (K) da proteína do fungo e uma fenilalanina (F) da proteína da formiga, com uma distância de 2.1 angstroms.
É uma interação entre um aminoácido polar básico e um aminoácido aromático, relativamente não polar.

3 - Interação entre uma treonina (T) da proteína do fungo e um triptofano (W) da proteína da formiga, com uma distância de 2.9 angstroms.
É uma interação entre um aminoácido polar e um aminoácido aromático, relativamente não polar.

4 - Interação entre um ácido aspártico (D) da proteína do fungo e uma arginina (R) da proteína da formiga, com uma distância de 3.3 angstroms.
É uma interação entre um aminoácido polar ácido e um aminoácido polar básico.

5 - Interação entre um ácido glutâmico (E) da proteína do fungo e uma fenilalanina (F) da proteína da formiga, com uma distância de 3.5 angstroms.
É uma interação entre um aminoácido polar ácido e um aminoácido aromático, relativamente não polar.


***********************************************Print das interações*********************************************************
 
O núcleo da isoleucina é o mais hidrófobo de todos os radicais dos aminoácidos das proteínas. Tais características hidrofóbicas permitem a formação de ligações hidrófobas que são consideradas as ligações mais fracas.
A lisina contém uma cadeia lateral muito polar, o que a torna altamente hidrófilica. Este aminoácido tem carga positiva em pH neutro.
O triptofano e a fenilalanina são aminoácidos aromáticos que formam ligações π que são geralmente fortes.

Na 1ª interação observamos a ligação entre o grupo carboxilo da isoleucina e o anel aromático com nitrogénio (N) do triptofano. Essa ligação é formada através duma ponte de hidrogénio (O-H-----N). Estão também presentes nesta interação forças de dispersão de London. Entre as duas ligações, a ponte de hidrogénio é a mais forte.

No caso da segunda interação temos uma ligação não covalente do tipo catião - π, devido à cadeia lateral básica, carregada positivamente da lisina e do anel aromático presente na fenilalanina, uma ponte de hidrogénio, e interações de Debye e de London. Temos duas ligações não covalentes relativamente fortes: catião - π e ponte de hidrogénio. 

Na 3ª interação observamos uma ponte de hidrogénio entre uma treonina e um triptofano. Estão também presentes nesta interação forças de Debye e forças de dispersão de London. Entre estas ligações, a ponte de hidrogénio é a mais forte.

Na 4º interação existe uma ligação iónica (a ligação não covalente mais forte), pois o ácido aspártico tem carga negativa e a arginina tem carga positiva, uma ponte de hidrogénio, e interações de Van der Waals (Keesom, Debye e London), sendo a ligação iónica a mais forte.

Na quinta interação ocorre uma ligação anião - π devido à cadeia lateral ácida, carregada negativamente do ácido glutâmico, e do anel aromático presente na fenilalanina. Além disso temos uma ponte de hidrogénio, e forças de Debye e de London. A ponte de hidrogénio é a mais forte. 


Conforme as características dos aminoácidos envolvidos nas interações, e tendo em conta o número de interações encontradas no docking, o número de interações entre os aminoácidos, e as distâncias entre estes mesmos aminoácidos (menores de 4 angstrons), pode-se concluir que as afinidades químicas são fortes o suficientes para confirmar essa predição de interação proteína-proteína.

