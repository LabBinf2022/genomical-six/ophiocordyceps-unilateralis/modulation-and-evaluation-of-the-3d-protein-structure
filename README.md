# Modulation and Evaluation of the 3D protein structure 

This project is basically to know if two proteins interact with each other, the Ophiocordyceps unilateralis and the protein of a carpenter ant.

It's intended to obtain and visualize 3D structures proteins of the species in study trough methods of computacional prediction.

We're hoping to figure out how the two proteins interact and how is that related to the pathogenic effects that the ants feature.

The model_1.pdb is the docking that we used.